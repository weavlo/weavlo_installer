# weavlo_installer

This tool installs the necessary scaffolding for ERPNext.  It also installs the Frappe and ERPNext apps.

## Requirements

We need a single program that installs the following:

1. Scaffolding for Frappe framework. (Python virtual environments, core directories, etc.)
2. Frappe app
3. ERPNext app

### What about Python and Node packages?
When you download Frappe and ERPNext from GitHub, they contain package files for:
  * Python.  The script *setup.py* installs packages described in "requirements.txt" and/or "install_requires".  Most of these packages are fetched from  [PyPi](https://pypi.org/).
  * Node.  The file *package.json* describes the required packages.  Most of these are fetch from [NPM](https://www.npmjs.com/)
      
Handling these are a **critical** part of the project.  We must lock-down these PiPy and NPM packages, to prevent their version-creep from breaking Frappe and ERPNext.

The *weavlo_installer* will handle these.  But it needs to do a much better job than Bench.

## Supported Versions
The *weavlo_installer* will **only** support major, Long Term Support (LTS) versions of ERPNext.

It will not support the "develop", "staging", or various other git branches of Frappe or ERPNext.

## What This Program Does Not Do
This program does **not** install prerequisite software.  Do one thing; do it well.

All prerequisite software (MariaDB, Redis, Node, Nginx) should either be installed:
1.  Manually by the user.  This is desireable by large organizations, with strict IT controls on their software.
2.  Using [weavlo_reqinstaller](https://gitlab.com/weavlo/weavlo_reqinstaller)

This program **should** call [weavlo_checkreqs](https://gitlab.com/weavlo/weavlo_checkreqs), and verify all prerequisites are installed.  If not, this installer should provide feedback to the user, and exit gracefully.

## Additional Thoughts
This installer tool is the driving force behind the Weavlo project.  It's the face of Weavlo.

In the current ERPNext ecosystem, users either:
  * Run the so-called "Easy Install" Python script, which tries to do everything for them.
  * Or they install manually, which requires a working knowledge of Linux, Python, Bash, and other skills.

We need to abandon this line of thinking.  From a user-perspective, the installation needs to be incredibly simple.
1. Download installer executable.
2. Run installer executable.
3. Installer initiates a CLI or GUI interface.  Asks questions of the user.  And does its job.
4. Installer should not launch ERPNext.  Do one thing; do it well.  We don't want to replace [Supervisor](http://supervisord.org/introduction.html#overview), honcho, etc..

We should take a serious look at how similar installation tools work.

#### "Imitation is the sincerest form of flattery..." - Oscar Wilde
Cleanly installing software is not a new concept.  Let us not "reinvent the wheel."

How do other Python frameworks perform installation?  Let's learn from them:
* The [Drupal](https://www.drupal.org/) installation process is a nice example.